const REGEX = /^([A-Za-z]){1}/

export default function startWithChar (value) {
  return REGEX.test(String(value))
    ? false
    : "Value can't start with a number"
}