export default function lenBetween (min, max) {
  return (value) => {
    return value.length >= min && value.length <= max
      ? false
      : `Value must be between ${min} and ${max} characters-long`
  }
}