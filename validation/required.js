const ERROR_MESSAGE = "Value can't be blank"

export default function required(value) {
  if (value === undefined || value === null) {
    return ERROR_MESSAGE
  }
  return String(value).trim().length > 0
    ? false
    : ERROR_MESSAGE
}