export { default as alphaNum } from './alphaNum' 
export { default as email } from './email' 
export { default as lenBetween } from './lenBetween' 
export { default as required } from './required'
export { default as startWithChar } from './startWithChar'

const errorMessage = (rules, value) => {
  for (const rule of rules) {
    const outcome = rule(value)
    
    if (outcome) {
      return outcome
    }
  }

  return ''
}

export const validate = (rules, form) => {
  let hasError = false;
  const errors = {}

  for (const [field, fieldRules] of Object.entries(rules)) {
    errors[field] = errorMessage(fieldRules, form[field])
    hasError = hasError || Boolean(errors[field])
  }

  return {
    ...errors,
    isValid: !hasError 
  }
}