const REGEX = /^([A-Za-z0-9])*$/

export default function alphaNum (value) {
  return REGEX.test(value)
    ? false
    : 'Value should contain only alphanumeric characters'
}