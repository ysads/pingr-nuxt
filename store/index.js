import jwt from 'jsonwebtoken'

const API_URL = 'http://localhost:3001'

export const state = () => ({
  token: null,
  id: null,
})

export const actions = {
  async register ({ commit }, form) {
    const res = await this.$axios.post(`${API_URL}/register`, form)
    const { sub } = jwt.decode(res.data.accessToken)

    commit('SET_TOKEN', res.data.accessToken)
    commit('SET_ID', sub)

    return sub
  },

  async login ({ commit }, form) {
    const res = await this.$axios.post(`${API_URL}/login`, form)
    const { sub } = jwt.decode(res.data.accessToken)

    commit('SET_TOKEN', res.data.accessToken)
    commit('SET_ID', sub)

    return sub
  },

  async getUser ({ state }, id) {
    this.$axios.setToken(state.token, 'Bearer')

    const res = await this.$axios.get(`${API_URL}/600/users/${id}`)

    return res.data
  }
}

export const mutations = {
  SET_TOKEN (state, token) {
    state.token = token
  },

  SET_ID (state, id) {
    state.id = id
  },
}